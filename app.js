// Node js
const fs = require('fs')
// Express
const express = require('express')
const bodyParser = require('body-parser')
const app = express()
// router
const articlesRoute = require('./src/routes/articleRoute')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(articlesRoute)

const dataPath = './article.json'
if(!fs.existsSync(dataPath)){
   fs.writeFileSync(dataPath, '[]', 'utf-8')
}

app.get('/', (req, res) => {
   res.redirect('/articles')
})

app.listen(3000)