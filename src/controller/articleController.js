const fs = require('fs')
const { v4: uuidv4 } = require('uuid')

exports.createArticle = (req, res, next) => {
   const { title, content, author } = req.body
   const newArticle = { id: uuidv4(), title, content, author }
   const fileBuffer = fs.readFileSync('./article.json', 'utf-8')
   const articles = JSON.parse(fileBuffer)

   articles.push(newArticle)

   fs.writeFileSync('./article.json', JSON.stringify(articles))
   res.json({
      message: "berhasil membuat artikel",
      article: newArticle
   })
}

exports.getArticles = (req, res, next) => {
   fs.readFile('./article.json', 'utf-8', (err, jsonString) => {
      if(err){
         res.json({ message: "gagal mengambil artikell" })
         return
      }
      let articleList = JSON.parse(jsonString)
      res.json({ 
         message: "berhasil mengambil list artikel",
         data: articleList
      })
   })
}

exports.getArticlesById = (req, res, next) => {
   const id = req.params.id_article
   const articleBuffer = fs.readFileSync('./article.json', 'utf8')
   const articleList = JSON.parse(articleBuffer)
   const article = articleList.find((item) => item.id === id)
   res.json({ 
      message:`berhasil mengambil artikel dengan id ${id}`,
      data: article
   })
}

exports.deleteArticle = (req, res, next) => {
   const id = req.params.id_article
   const articleBuffer = fs.readFileSync('./article.json', 'utf8')

   const articleList = JSON.parse(articleBuffer)
   const articleIndex = articleList.findIndex((item) => item.id === id)
   articleList.splice(articleIndex, 1)

   fs.writeFileSync('./article.json', JSON.stringify(articleList))
   res.json({ message: `berhasil menghapus artikel dengan id ${id}` })
}