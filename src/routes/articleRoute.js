const express = require('express')

const articleController = require('../controller/articleController')
const router = express.Router()
const _ = articleController
// create article
router.post('/articles', _.createArticle)

// get all articles
router.get('/articles', _.getArticles)

// get single article by id
router.get('/articles/:id_article', _.getArticlesById)

// delete articles
router.delete('/articles/:id_article', _.deleteArticle)

module.exports = router